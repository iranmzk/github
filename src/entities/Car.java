package entities;

public class Car {
	
	private String name;
	private Integer year;
	private String color;

	public Car() {
		
	};
	public Car(String name, Integer year, String color) {
		this.name = name;
		this.year = year;
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	public String toString() {
		return "Model: " + name + ", " + "Year: "+ year + ", " + "Color: " + color;
	}
	}
